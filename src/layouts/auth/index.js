import React, { useState } from "react";
import { Route, Switch } from "react-router-dom";
// Chakra imports
import { Box } from "@chakra-ui/react";
// Layout components
import { SidebarContext } from "contexts/SidebarContext";
import SignIn from "views/auth/signIn/SignInDefault";

// Custom Chakra theme
export default function Auth() {
  const [toggleSidebar, setToggleSidebar] = useState(false);

  return (
    <>
      <Box>
        <SidebarContext.Provider
          value={{
            toggleSidebar,
            setToggleSidebar,
          }}
        >
          <Box
            float="right"
            minHeight="100vh"
            height="100%"
            position="relative"
            w="100%"
            transition="all 0.33s cubic-bezier(0.685, 0.0473, 0.346, 1)"
            transitionDuration=".2s, .2s, .35s"
            transitionProperty="top, bottom, width"
            transitionTimingFunction="linear, linear, ease"
          >
            <Box mx="auto" minH="100vh">
              <Switch>
                <Route path="/auth/sign-in" component={SignIn} />
              </Switch>
            </Box>
          </Box>
        </SidebarContext.Provider>
      </Box>
    </>
  );
}
