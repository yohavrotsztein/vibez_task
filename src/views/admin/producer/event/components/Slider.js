import React, { useState, useEffect } from "react";

import {
  Slider,
  SliderTrack,
  SliderFilledTrack,
  SliderThumb,
  SliderMark,
  Tooltip,
} from "@chakra-ui/react";

export default function SliderThumbWithTooltip({ value, set_value }) {
  const [sliderValue, setSliderValue] = useState(0);
  const [showTooltip, setShowTooltip] = useState(false);

  const handleChange = (value) => {
    setSliderValue(value);
    set_value(value);
  };

  useEffect(() => {
    if (value) setSliderValue(value);
  }, [value]);

  return (
    <Slider
      id="slider"
      defaultValue={sliderValue}
      value={sliderValue}
      step={10}
      min={0}
      max={100}
      colorScheme="teal"
      onChange={(v) => handleChange(v)}
      onMouseEnter={() => setShowTooltip(true)}
      onMouseLeave={() => setShowTooltip(false)}
    >
      <SliderMark value={20} mt="1" ml="-2.5" fontSize="sm">
        20%
      </SliderMark>
      <SliderMark value={50} mt="1" ml="-2.5" fontSize="sm">
        50%
      </SliderMark>
      <SliderMark value={80} mt="1" ml="-2.5" fontSize="sm">
        80%
      </SliderMark>
      <SliderTrack>
        <SliderFilledTrack />
      </SliderTrack>
      <Tooltip
        hasArrow
        bg="teal.500"
        color="white"
        placement="top"
        isOpen={showTooltip}
        label={`${sliderValue}%`}
      >
        <SliderThumb />
      </Tooltip>
    </Slider>
  );
}
