/*!
  _   _  ___  ____  ___ ________  _   _   _   _ ___   ____  ____   ___  
 | | | |/ _ \|  _ \|_ _|__  / _ \| \ | | | | | |_ _| |  _ \|  _ \ / _ \ 
 | |_| | | | | |_) || |  / / | | |  \| | | | | || |  | |_) | |_) | | | |
 |  _  | |_| |  _ < | | / /| |_| | |\  | | |_| || |  |  __/|  _ <| |_| |
 |_| |_|\___/|_| \_\___/____\___/|_| \_|  \___/|___| |_|   |_| \_\\___/ 
                                                                                                                                                                                                                                                                                                                                       
=========================================================
* Horizon UI Dashboard PRO - v1.0.0
=========================================================

* Product Page: https://www.horizon-ui.com/pro/
* Copyright 2022 Horizon UI (https://www.horizon-ui.com/)

* Designed and Coded by Simmmple

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/

import React from "react";

// Chakra imports
import {
  Flex,
  useColorModeValue,
  SimpleGrid,
  Button,
  Text,
  InputGroup,
  Select,
  InputLeftElement,
  Input,
} from "@chakra-ui/react";
import { Formik, Form } from "formik";
import Card from "components/card/Card.js";
import InputField from "components/fields/InputFormikField";
import TextField from "components/fields/TextField";
import Dropzone from "./Dropzone";

// Custom components
import { PhoneIcon, EmailIcon } from "@chakra-ui/icons";

export default function FormikForm({ activeMode, setShowForm }) {
  const textColorPrimary = useColorModeValue("secondaryGray.900", "white");
  const textColorSecondary = "secondaryGray.600";

  // Chakra Color Mode
  return (
    <Formik
      initialValues={{
        name: "",
        address: "",
      }}
      onSubmit={(values) => {
        console.log(values);
      }}
    >
      {({ errors, touched, isValidating }) => (
        <Form w="60%" mt={"20px"} isDisabled={activeMode === 1 ? false : true}>
          <Card mb="20px">
            <InputField
              mb="10px"
              me="30px"
              id="name"
              name="name"
              label="Project name"
              placeholder="The best project"
            />
            <InputField
              mb="10px"
              id="address"
              name="address"
              label="Location"
              placeholder="Dizengoff 1, Tel Aviv"
            />
            <Text
              fontSize="sm"
              color={activeMode === 1 ? textColorPrimary : textColorSecondary}
              fontWeight="bold"
              marginInlineStart={"10px"}
              mb={"8px"}
            >
              Currency
            </Text>
            <Select name="currency" mb="10px" me="30px" borderRadius={"16px"}>
              <option>ILS</option>
              <option>USD</option>
              <option>EUR</option>
            </Select>
            <Text
              fontSize="sm"
              color={activeMode === 1 ? textColorPrimary : textColorSecondary}
              fontWeight="bold"
              marginInlineStart={"10px"}
              mb={"8px"}
            >
              Logo
            </Text>
            <Dropzone name="logo" />
            <Text
              fontSize="sm"
              color={activeMode === 1 ? textColorPrimary : textColorSecondary}
              fontWeight="bold"
              marginInlineStart={"10px"}
              mb={"8px"}
            >
              Cover
            </Text>
            <Dropzone name="cover" />
            <TextField
              id="description"
              name="description"
              label="Description"
              h="100px"
              placeholder="Tell something about yourself in 150 characters!"
            />
            <Text
              fontSize="sm"
              color={activeMode === 1 ? textColorPrimary : textColorSecondary}
              fontWeight="bold"
              marginInlineStart={"10px"}
              mb={"8px"}
            >
              Contacts
            </Text>

            <SimpleGrid
              columns={{ sm: 1, md: 2 }}
              spacing={{ base: "20px", xl: "20px" }}
            >
              <InputGroup>
                <InputLeftElement
                  pointerEvents="none"
                  children={<PhoneIcon color="gray.300" borderRadius="16px" />}
                />
                <Input
                  mb="0px"
                  borderRadius="16px"
                  // me="30px"
                  id="phone"
                  name="phone"
                  placeholder="Add phone number"
                />
              </InputGroup>
              <InputGroup>
                <InputLeftElement
                  pointerEvents="none"
                  children={<PhoneIcon color="gray.300" borderRadius="16px" />}
                />
                <Input
                  mb="0px"
                  borderRadius="16px"
                  id="facebook"
                  name="facebook"
                  placeholder="Add your Facebook Page"
                />
              </InputGroup>
              <InputGroup>
                <InputLeftElement
                  pointerEvents="none"
                  children={<PhoneIcon color="gray.300" borderRadius="16px" />}
                />
                <Input
                  mb="20px"
                  // me="30px"
                  borderRadius="16px"
                  id="instagram"
                  name="instagram"
                  placeholder="Add your instagram account"
                />
              </InputGroup>
              <InputGroup>
                <InputLeftElement
                  pointerEvents="none"
                  children={<EmailIcon color="gray.300" borderRadius="16px" />}
                />
                <Input
                  mb="20px"
                  borderRadius="16px"
                  id="email"
                  name="email"
                  placeholder="Add your email"
                />
              </InputGroup>
            </SimpleGrid>
            {activeMode === 1 && (
              <Flex direction={"row-reverse"} ms="10px">
                <Button
                  onClick={() => setShowForm(0)}
                  variant={"outline"}
                  minW={"100px"}
                  size="md"
                  fontWeight="500"
                  borderRadius="70px"
                >
                  Cancel
                </Button>
                <Button
                  type="submit"
                  variant={"brand"}
                  minW={"100px"}
                  size="md"
                  fontWeight="500"
                  borderRadius="70px"
                  mx="10px"
                >
                  Save
                </Button>
              </Flex>
            )}
          </Card>
        </Form>
      )}
    </Formik>
  );
}
