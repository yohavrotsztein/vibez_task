/*!
  _   _  ___  ____  ___ ________  _   _   _   _ ___   ____  ____   ___  
 | | | |/ _ \|  _ \|_ _|__  / _ \| \ | | | | | |_ _| |  _ \|  _ \ / _ \ 
 | |_| | | | | |_) || |  / / | | |  \| | | | | || |  | |_) | |_) | | | |
 |  _  | |_| |  _ < | | / /| |_| | |\  | | |_| || |  |  __/|  _ <| |_| |
 |_| |_|\___/|_| \_\___/____\___/|_| \_|  \___/|___| |_|   |_| \_\\___/ 
                                                                                                                                                                                                                                                                                                                                       
=========================================================
* Horizon UI Dashboard PRO - v1.0.0
=========================================================

* Product Page: https://www.horizon-ui.com/pro/
* Copyright 2022 Horizon UI (https://www.horizon-ui.com/)

* Designed and Coded by Simmmple

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/

import React, { useState, useEffect } from "react";

// Chakra imports
import {
  Flex,
  SimpleGrid,
  FormControl,
  Button,
  Image,
  Text,
  InputGroup,
  InputLeftElement,
  Input,
} from "@chakra-ui/react";
import FormData from "form-data";
import { useHistory } from "react-router-dom";

import Card from "components/card/Card.js";
import InputField from "components/fields/InputField";
import TextField from "components/fields/TextField";
import Dropzone from "./Dropzone";

// Custom components
import Facebook from "assets/svg/icons/facebook.svg";
import Email from "assets/svg/icons/email.svg";
import Instagram from "assets/svg/icons/instagram.svg";
import { useAuthContext } from "hooks/useAuthContext";
import { DeleteButton } from "components/buttons/DeleteButton";
import PhoneField from "components/fields/PhoneField";
import Label from "components/fields/Label";
import { Spinner } from "components/spinner";

export default function FormCard({ project, setProject, setShowForm }) {
  const { user } = useAuthContext();
  let history = useHistory();

  const [currentProject, setCurrentProject] = useState(null);
  const [name, setName] = useState(project?.name);
  const [address, setAddress] = useState(project?.address);
  const [currency, setCurrency] = useState(project ? project.currency : "ILS");
  const [logo, setLogo] = useState(project?.logo ? project?.logo : null);
  const [newLogo, setNewLogo] = useState(null);
  const [cover, setCover] = useState(project?.cover ? project.cover : null);
  const [newCover, setNewCover] = useState(null);
  const [description, setDescription] = useState(project?.description);
  const [tags, setTags] = useState([]);
  const [whatsapp, setWhatsapp] = useState(
    project?.contacts?.whatsapp
      ? "+" + project?.contacts?.whatsapp.replace(/\D/g, "")
      : null
  );
  const [facebook, setFacebook] = useState(project?.contacts?.facebook);
  const [instagram, setInstagram] = useState(project?.contacts?.instagram);
  const [email, setEmail] = useState(project?.contacts?.email);
  const [orgName, setOrgName] = useState(project?.billing_details?.org_name);
  const [orgNumber, setOrgNumber] = useState(
    project?.billing_details?.org_number
  );
  const [orgAddress, setOrgAddress] = useState(
    project?.billing_details?.org_address
  );
  const [country, setCountry] = useState(
    project?.billing_details?.country
      ? project?.billing_details?.country
      : "Israel"
  );
  const [errorMsg, setErrorMsg] = useState("");
  const [inProgress, setInProgress] = useState(false);

  const showError = () => {
    setErrorMsg("You can't delete the Project");
    setInterval(() => {
      setErrorMsg("");
    }, 2000);
  };

  useEffect(() => {
    if (project) setCurrentProject(project);
  }, [project]);

  const uploadImage = async (end_point, file, wait) => {
    let path = process.env.REACT_APP_API_URL + "/projects/" + end_point;
    const formData = new FormData();
    formData.append("file", file);
    console.log("UPLOAD BODY: ", file);

    try {
      const response = await fetch(path, {
        method: "POST",
        headers: {
          Authorization: user,
        },
        body: formData,
      });
      if (response) console.log("UPLOAD RES: ", response);
      if (response.ok && !wait) {
        setInProgress(false);
        window.location.reload(false);
      }
    } catch (e) {
      throw new Error(e);
    }
  };

  const handleCancel = () => {
    if (!project) {
      setShowForm(false);
      return;
    }
    setName(project?.name);
    setAddress(project?.address);
    setCurrency(project ? project.currency : "ILS");
    setLogo(project?.logo);
    setNewLogo(null);
    setCover(project?.cover ? project.cover : null);
    setNewCover(null);
    setDescription(project?.description);
    setTags([]);
    setWhatsapp(
      project?.contacts?.whatsapp
        ? "+" + project?.contacts?.whatsapp.replace(/\D/g, "")
        : null
    );
    setFacebook(project?.contacts?.facebook);
    setInstagram(project?.contacts?.instagram);
    setEmail(project?.contacts?.email);
    setOrgName(project?.billing_details?.org_name);
    setOrgNumber(project?.billing_details?.org_number);
    setOrgAddress(project?.billing_details?.org_address);
    setCountry(project?.billing_details?.country);
  };

  const handleSave = async () => {
    if (inProgress) return;
    setErrorMsg("");

    if (!user) return;

    if (!country) {
      setErrorMsg("Please provide billing details: Country");
      return;
    }
    if (!orgAddress) {
      setErrorMsg("Please provide billing details: Organization address");
      return;
    }
    if (!orgName) {
      setErrorMsg("Please provide billing details: Organization name");
      return;
    }
    if (!orgNumber) {
      setErrorMsg("Please provide billing details: Organization Number");
      return;
    }
    if (!project?.cover && !newCover) {
      setErrorMsg("Can't save project without cover");
      return;
    }
    if (!project?.logo && !newLogo) {
      setErrorMsg("Can't save project without logo");
      return;
    }
    if (!email) {
      setErrorMsg("Can't save project without email");
      return;
    }
    setInProgress(true);

    const body = {
      project: {
        name: name,
        address: address,
        description: description,
        currency: currency,
        contacts: {
          email: email,
          instagram: instagram,
          facebook: facebook,
          whatsapp: whatsapp
            ? "http://wa.me/" + whatsapp.replace(/\D/g, "")
            : whatsapp,
        },
        billing_details: {
          org_name: orgName,
          org_number: orgNumber,
          org_address: orgAddress,
          country: country,
        },
        status: "active",
      },
    };
    console.log("CREATE PROJECT BODY: ", JSON.stringify(body));
    let path =
      process.env.REACT_APP_API_URL +
      "/projects" +
      (project ? "/" + project.id : "");
    const response = await fetch(path, {
      method: project ? "PUT" : "POST",
      headers: {
        "Content-Type": "application/json",
        Authorization: user,
      },
      body: JSON.stringify(body),
    });
    const json = await response.json();
    console.log("JSON: ", JSON.stringify(json));

    if (!response.ok) {
      setInProgress(false);
      setErrorMsg(json.error.message);
    }
    if (response.ok) {
      if (!newCover && !newLogo) {
        setCurrentProject(json.project);
        console.log("RES: ", JSON.stringify(json));
        setInProgress(false);
        window.location.reload(false);
      }
      if (newLogo) uploadImage(json.project.id + "/logo", newLogo, newCover);
      if (newCover) uploadImage(json.project.id + "/cover", newCover);
    }
    return;
  };

  const handleDelete = async () => {
    if (inProgress) return;
    if (!user) return;
    setInProgress(true);
    let path = process.env.REACT_APP_API_URL + "/projects/" + project.id;
    const response = await fetch(path, {
      method: "DELETE",
      headers: {
        "Access-Control-Allow-Credentials": true,
        "Content-Type": "application/json",
        Authorization: user,
      },
    });
    const json = await response.json();
    if (response.ok) {
      console.log("DELETE PROJECT RES: ", json);
      setInProgress(false);
      history.push("/projects");
    } else {
      setInProgress(false);
      showError();
    }
  };

  useEffect(() => {
    console.log("LOGO: ", logo);
  }, [logo]);

  return (
    <Flex
      direction={"column"}
      w="100%"
      justifyContent={"center"}
      alignItems={"center"}
    >
      <Text color="red" pt="20px">
        {errorMsg}
      </Text>
      <FormControl w="60%" minW="300px" mt={"20px"}>
        <Card mb="20px">
          <InputField
            mb="10px"
            me="30px"
            id="name"
            label="Project name"
            placeholder="The best project"
            required={true}
            value={name}
            onChange={(e) => setName(e.target.value)}
          />
          <InputField
            mb="10px"
            id="location"
            label="Location"
            placeholder="Dizengoff 1, Tel Aviv"
            required={true}
            value={address}
            onChange={(e) => setAddress(e.target.value)}
          />
          {/* <Text
          fontSize="sm"
          color={textColor}
          fontWeight="bold"
          marginInlineStart={"10px"}
          mb={"8px"}
        >
          Currency
        </Text>
        <Select
          onChange={(e) => setCurrency(e.target.value)}
          mb="10px"
          me="30px"
          borderRadius={"16px"}
          value={currency}
        >
          <option value="ILS">ILS</option>
          <option value="USD">USD</option>
          <option value="EUR">EUR</option>
        </Select> */}
          <Label label={"Logo"} required={true} />
          {logo && !newLogo ? (
            <Flex
              mb={{ base: "20px", "2xl": "20px" }}
              justifyContent={"center"}
            >
              <Image
                src={logo}
                w={{ base: "50%", "3xl": "50%" }}
                h={{ base: "50%", "3xl": "50%" }}
                borderRadius="20px"
              />
            </Flex>
          ) : null}
          <Dropzone
            action={setNewLogo}
            instructions={
              "Please make sure to upload *.jpeg and *.png Image 1:1 (or size 200x200 px)"
            }
          />
          <Label label={"Cover"} required={true} />
          {cover && !newCover ? (
            <Flex
              mb={{ base: "20px", "2xl": "20px" }}
              justifyContent={"center"}
            >
              <Image
                src={cover}
                w={{ base: "50%", "3xl": "50%" }}
                h={{ base: "50%", "3xl": "50%" }}
                borderRadius="20px"
              />
            </Flex>
          ) : null}
          <Dropzone
            action={setNewCover}
            instructions={
              "Please make sure to upload *.jpeg and *.png Image 9:16 (or size 1200x675 px)"
            }
          />
          <TextField
            onChange={(e) => setDescription(e.target.value)}
            value={description}
            id="description"
            label="Description"
            required={true}
            h="100px"
            placeholder="Tell something about project in 150 characters!"
          />
          <Label label={"Contacts"} />
          <Label label={"Add whatsapp number"} />
          <SimpleGrid
            columns={{ sm: 1, md: 2 }}
            spacing={{ base: "20px", xl: "20px" }}
          >
            <InputGroup>
              {/* <InputLeftElement
                pointerEvents="none"
                children={
                  <Image
                    src={Whatsapp}
                    width="20px"
                    height="20px"
                    mr="10px"
                    ml="10px"
                  />
                }
              /> */}
              <PhoneField
                value={whatsapp}
                setValue={setWhatsapp}
                // marginInlineStart="50px"
                mb="0px"
                h="40px"
              />
              {/* <Input
                mb="0px"
                borderRadius="16px"
                id="phone"
                placeholder="Add your Whatsapp link"
                value={whatsapp}
                onChange={(e) => setWhatsapp(e.target.value)}
              /> */}
            </InputGroup>
            <InputGroup>
              <InputLeftElement
                pointerEvents="none"
                children={
                  <Image
                    src={Facebook}
                    width="20px"
                    height="20px"
                    mr="10px"
                    ml="10px"
                  />
                }
              />
              <Input
                mb="0px"
                borderRadius="16px"
                id="facebook"
                placeholder="Add your Facebook Page"
                value={facebook}
                onChange={(e) => setFacebook(e.target.value)}
              />
            </InputGroup>
            <InputGroup>
              <InputLeftElement
                pointerEvents="none"
                children={
                  <Image
                    src={Instagram}
                    width="20px"
                    height="20px"
                    mr="10px"
                    ml="10px"
                  />
                }
              />
              <Input
                mb="20px"
                borderRadius="16px"
                id="instagram"
                placeholder="Add your instagram account"
                value={instagram}
                onChange={(e) => setInstagram(e.target.value)}
              />
            </InputGroup>
            <InputGroup>
              <InputLeftElement
                pointerEvents="none"
                children={
                  <Image
                    src={Email}
                    width="20px"
                    height="20px"
                    mr="10px"
                    ml="10px"
                  />
                }
              />
              <Input
                mb="20px"
                borderRadius="16px"
                id="email"
                placeholder="Add your email*"
                value={email}
                onChange={(e) => setEmail(e.target.value)}
              />
            </InputGroup>
          </SimpleGrid>
          <Label label={"Billing Details"} required={true} />
          <InputField
            mb="10px"
            id="org_address"
            label="Organization name"
            placeholder=""
            change
            date
            time
            picker
            value={orgName}
            onChange={(e) => setOrgName(e.target.value)}
          />
          <SimpleGrid
            columns={{ sm: 1, md: 2 }}
            spacing={{ base: "20px", xl: "20px" }}
          >
            <InputField
              mb="10px"
              id="org_address"
              label="Country"
              placeholder=""
              value={country}
              onChange={(e) => setCountry(e.target.value)}
            />
            <InputField
              mb="10px"
              id="org_address"
              label="Organization number"
              placeholder=""
              value={orgNumber}
              onChange={(e) => setOrgNumber(e.target.value)}
            />
          </SimpleGrid>
          <InputField
            mb="10px"
            id="org_address"
            label="Organization Address"
            placeholder=""
            value={orgAddress}
            onChange={(e) => setOrgAddress(e.target.value)}
          />
          <Text color="red">{errorMsg}</Text>
          {inProgress ? (
            <Spinner />
          ) : (
            <Flex
              direction={{ base: "column-reverse", md: "row" }}
              justifyContent={"center"}
              alignItems={"center"}
              mx="10px"
              mt="40px"
            >
              <DeleteButton
                action={() => {
                  handleDelete();
                }}
                isPassive={true}
              />
              <Button
                onClick={handleCancel}
                variant={"outline"}
                minW={"100px"}
                size="md"
                fontWeight="500"
                borderRadius="70px"
                mx="5px"
              >
                Cancel
              </Button>
              <Button
                onClick={() => {
                  handleSave();
                }}
                variant={"brand"}
                minW={"100px"}
                size="md"
                fontWeight="500"
                borderRadius="70px"
                mx="5px"
              >
                Save
              </Button>
            </Flex>
          )}
        </Card>
      </FormControl>
    </Flex>
  );
}
