/*!
  _   _  ___  ____  ___ ________  _   _   _   _ ___   ____  ____   ___  
 | | | |/ _ \|  _ \|_ _|__  / _ \| \ | | | | | |_ _| |  _ \|  _ \ / _ \ 
 | |_| | | | | |_) || |  / / | | |  \| | | | | || |  | |_) | |_) | | | |
 |  _  | |_| |  _ < | | / /| |_| | |\  | | |_| || |  |  __/|  _ <| |_| |
 |_| |_|\___/|_| \_\___/____\___/|_| \_|  \___/|___| |_|   |_| \_\\___/ 
                                                                                                                                                                                                                                                                                                                                       
=========================================================
* Horizon UI Dashboard PRO - v1.0.0
=========================================================

* Product Page: https://www.horizon-ui.com/pro/
* Copyright 2022 Horizon UI (https://www.horizon-ui.com/)

* Designed and Coded by Simmmple

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/

import React, { useState, useEffect } from "react";

// Chakra imports
import { Flex, Text } from "@chakra-ui/react";

// Custom components
import { columnsDataProducts } from "./components/ColumnsData";
import { AddButton } from "components/buttons/AddButton";
// Assets
import { ProductsTable } from "./components/ProductsTable";
import { ProductPopup } from "./components/ProductPopup";
import { Spinner } from "components/spinner";

export default function Products({ event_path, user }) {
  const [products, setProducts] = useState([]);
  const [productPath, setProductPath] = useState("");
  const [error, setError] = useState(null);
  const [showDetails, setShowDetails] = useState(false);
  const [inProgress, setInProgress] = useState(false);

  const fetchProducts = async (path) => {
    if (inProgress) return;
    if (!user) return;
    setInProgress(true);
    const response = await fetch(path, {
      method: "GET",
      headers: {
        "Access-Control-Allow-Credentials": true,
        "Content-Type": "application/json",
        Authorization: user,
      },
    });
    const json = await response.json();
    console.log("GET PRODUCTS RES: ", json);
    console.log("%cProduct fetch",'background: #222; color: #bada55');

    setInProgress(false);
    if (response.ok) {
      setProducts(json.products);
    } else {
      setError(response?.error?.message);
    }
  };

  const toggleClosure = () => {
    setShowDetails(!showDetails);
  };


  const updateProduct = (item) => {
    const updatedProduct = products.findIndex(
      (product) => item.id === product.id
    );

    if (updatedProduct !== -1) {
      products.splice(updatedProduct, 1, item);
      setProducts([...products]);
    } else {
      setProducts([...products, item]);
    }
  };
  // const updateProduct = (item) => {

  //   setProducts(items => {
  //     return items.map(product => product?.id === item?.id ? item : product)
  //   })
  // }




  const renderProductPopup = () => {
    if (showDetails)
      return (
        <ProductPopup
          path={productPath}
          user={user}
          toggleClosure={toggleClosure}
          fetchProducts={fetchProducts}
          updateProduct={updateProduct}
        />
      );
  };

  useEffect(() => {
    localStorage.setItem("event_route", "products");
    // if (user && event_path) {
    if (user && event_path && products.length === 0) {
      let path = event_path + "/products";
      setProductPath(path);
      fetchProducts(path);
    }
  // eslint-disable-next-line
  }, [user, products.length, event_path]);

  // Chakra Color Mode
  return (
    <Flex
      w="100%"
      justifyContent={"center"}
      direction={"column"}
      alignItems={"center"}
    >
      <Flex
        w="100%"
        marginBottom={"10px"}
        justifyContent={"center"}
        paddingEnd="40px"
      >
        <AddButton text={"Create Product"} action={toggleClosure} />
      </Flex>
      {error && <Text color="red">{error}</Text>}
      {/* {showForm > 0 && (
        <FormCard
          user={user}
          path={productPath}
          fetchProducts={fetchProducts}
        />
      )} */}
      {renderProductPopup()}
      {inProgress && <Spinner/>}
      {products && products.length > 0 && (
        <ProductsTable
          user={user}
          setError={setError}
          products={products}
          columnsData={columnsDataProducts}
          fetchProducts={fetchProducts}
          tableData={products}
          path={productPath}
          updateProduct={updateProduct}
        />
      )}
    </Flex>
  );
}
