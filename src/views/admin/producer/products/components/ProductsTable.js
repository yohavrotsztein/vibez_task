import {
  Button,
  Flex,
  Table,
  Tbody,
  Text,
  Th,
  Thead,
  Tr,
  useColorModeValue,
} from "@chakra-ui/react";
import React, { useMemo, useEffect, useState } from "react";
import {
  useGlobalFilter,
  usePagination,
  useSortBy,
  useTable,
} from "react-table";

// Custom components
import Card from "components/card/Card";
// Assets
import { product_type_dict, currency_dict } from "dictionaries";
import { ProductPopup } from "./ProductPopup";

export function ProductsTable(props) {
  const { user, fetchProducts, columnsData, tableData, path, updateProduct } = props;

  const columns = useMemo(() => columnsData, [columnsData]);
  const data = useMemo(() => tableData, [tableData]);

  const tableInstance = useTable(
    {
      columns,
      data,
    },
    useGlobalFilter,
    useSortBy,
    usePagination
  );

  const {
    getTableProps,
    getTableBodyProps,
    headerGroups,
    initialState,
  } = tableInstance;
  initialState.pageSize = 10;

  const textColor = useColorModeValue("secondaryGray.900", "white");
  const borderColor = useColorModeValue("gray.200", "whiteAlpha.100");
  const [showDetails, setShowDetails] = useState(false);
  const [product, setProduct] = useState();

  const toggleClosure = (item) => {
    setProduct(item);
    setShowDetails(!showDetails);
  };

  const renderProductPopup = () => {
    if (showDetails)
      return (
        <ProductPopup
          path={path}
          user={user}
          product={product}
          toggleClosure={toggleClosure}
          fetchProducts={fetchProducts}
          updateProduct={updateProduct}
        />
      );
  };

  useEffect(() => {
    console.log("OPEN: ", showDetails);
  }, [showDetails]);

  const renderBody = () => {
    return (
      data &&
      data.map((item) => {
        return (
          <tr key={item.id}>
            <td>
              <Flex align="center" marginInline={"24px"} marginTop="32px">
                <Text
                  me="10px"
                  color={textColor}
                  fontSize="sm"
                  fontWeight="700"
                >
                  {item?.name}
                </Text>
              </Flex>
            </td>
            <td>
              <Flex align="center" marginInline={"24px"} marginTop="32px">
                <Text
                  me="10px"
                  color={textColor}
                  fontSize="sm"
                  fontWeight="700"
                >
                  {item?.status}
                </Text>
              </Flex>
            </td>
            <td>
              <Flex align="center" marginInline={"24px"} marginTop="32px">
                <Text
                  me="10px"
                  color={textColor}
                  fontSize="sm"
                  fontWeight="700"
                >
                  {product_type_dict[item?.product_type]}
                </Text>
              </Flex>
            </td>
            <td>
              <Flex align="center" marginInline={"24px"} marginTop="32px">
                <Text
                  me="10px"
                  color={textColor}
                  fontSize="sm"
                  fontWeight="700"
                >
                  {item?.description}
                </Text>
              </Flex>
            </td>
            <td>
              <Flex align="center" marginInline={"24px"} marginTop="32px">
                <Text
                  me="10px"
                  color={textColor}
                  fontSize="sm"
                  fontWeight="700"
                >
                  {currency_dict["ILS"]}
                  {item?.price}
                </Text>
              </Flex>
            </td>
            <td>
              <Flex align="center" marginInline={"24px"} marginTop="32px">
                <Text
                  me="10px"
                  color={textColor}
                  fontSize="sm"
                  fontWeight="700"
                >
                  {item?.stock}
                </Text>
              </Flex>
            </td>
            <td>
              <Flex align="center" marginInline={"24px"} marginTop="32px">
                <Text
                  me="10px"
                  color={textColor}
                  fontSize="sm"
                  fontWeight="700"
                >
                  {item?.reserved}
                </Text>
              </Flex>
            </td>
            <td>
              <Flex align="center" marginInline={"24px"} marginTop="32px">
                <Text
                  me="10px"
                  color={textColor}
                  fontSize="sm"
                  fontWeight="700"
                >
                  {item?.sold}
                </Text>
              </Flex>
            </td>

            <td>
              <Flex align="center" marginInline={"24px"} marginTop="32px">
                <Button
                  onClick={() => toggleClosure(item)}
                  variant={"brand"}
                  minW={"100px"}
                  size="md"
                  fontWeight="500"
                  borderRadius="70px"
                  mx="10px"
                >
                  Edit
                </Button>
              </Flex>
            </td>
          </tr>
        );
      })
    );
  };

  return (
    <Card
      direction="column"
      w="100%"
      px="0px"
      overflowX={{ sm: "scroll", lg: "hidden" }}
    >
      {renderProductPopup()}
      <Table {...getTableProps()} variant="simple" color="gray.500" mb="24px">
        <Thead>
          {headerGroups.map((headerGroup, index) => (
            <Tr {...headerGroup.getHeaderGroupProps()} key={index}>
              {headerGroup.headers.map((column, index) => (
                <Th
                  {...column.getHeaderProps(column.getSortByToggleProps())}
                  pe="10px"
                  key={index}
                  borderColor={borderColor}
                >
                  <Flex
                    justify="space-between"
                    align="center"
                    fontSize={{ sm: "10px", lg: "12px" }}
                    color="gray.400"
                  >
                    {column.render("Header")}
                  </Flex>
                </Th>
              ))}
            </Tr>
          ))}
        </Thead>
        <Tbody {...getTableBodyProps()}>{renderBody()}</Tbody>
      </Table>
    </Card>
  );
}
